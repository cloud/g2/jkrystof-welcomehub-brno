# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


# Project
The `Welcome-hub base` provides
- container image providing web-server for run of the Welcome-hub. It is located in folder `container_image`. 
- Skeleton of the Welcome-hub page which should be enhanced by content for specific sites. It is located in folder `welcomehub_base`.
  

## [1.0.1]
 - Initial version based on Nginx web-server and site-neutral skeleton of the Welcome-hub.